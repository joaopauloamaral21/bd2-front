import { RouterModule, Routes } from '@angular/router';
import { AppComponent } from './app.component';
import { NgModule } from '@angular/core';
import { LoginComponent } from './components/login/login.component';
import { InitComponent } from './components/init/init.component';

const route: Routes = [

    {
        path: '', 
        component: LoginComponent
    },
    {
        path: 'home', 
        component: InitComponent
    }
]

@NgModule({
    imports: [
      RouterModule.forRoot(route)
    ],
    exports: [
      RouterModule
    ]})

export class AppRouteModule{};