import { Component, OnInit } from '@angular/core';
import { ServicesService } from '../../services/services.service';
import { product } from '../../models/product';
import { stock } from '../../models/stock';

@Component({
  selector: 'app-init',
  templateUrl: './init.component.html',
  styleUrls: ['./init.component.css']
})
export class InitComponent implements OnInit {

  products: Array<product> = [];
  stocks: Array<stock> = [];
  indexArray: number;
  type: string;
  header: any;

  constructor(private service: ServicesService) { }

  ngOnInit() {
    this.service.getHeader(s => {
      console.log(s);
      this.header = s;
    })
  }

  direct(type: string){
    this.type = type;
  }

  edit(index:number){
    this.indexArray = index;
  }

  ifEdit(index){
    return this.indexArray === index;
  }

  ifTipo(tipo: string){
    this.ifGetTipo();
    return this.type === tipo;
  }

  ifGetTipo(){
    if(this.type ==='produto'){
     this.getProdutos();
    }

    if(this.type ==='estoque'){
      this.getEstoque();
    }
  }

  getProdutos() {
    this.service.getForContext(this.header, 'produto').subscribe(products => {
      this.products = products;
    }, err=> {
      console.log(err);
    })
  }

  getEstoque() {
    this.service.getForContext(this.header, 'estoque').subscribe(stocks => {
      this.stocks = stocks;
    }, err=> {
      console.log(err);
    })
  }

  editForContext(date1, date2, id){
    const data = this.setObjectEdit(date1, date2, id);
    this.service.putForContext(this.header, 'produto', data).subscribe(s => {
      this.ifGetTipo();
    }, err =>  {
      console.log(err);
    });
  }

  setObjectEdit( data1, data2, id){
    if(this.type === 'produtos'){
      return {
        id: id,
        nome: data1,
        valor: data2
      }
    }else if(this.type === 'estoque'){
     return{
        id: id,
        gondola: data1,
        real: data2
     }
    }
  }

  newProduct(newName: string, newValue: string){
    const product = {
      nome: newName,
      valor: newValue
    }
    this.service.postForContext(this.header, 'produto', product).subscribe(s => {
      console.log(s);
      this.getProdutos();
    }, err =>  {
      console.log(err);
    });
  }

  delete(id:number, context:string) {
    this.service.deleteForContext(this.header,context, id).subscribe(s =>{
      this.ifGetTipo();
    });
  }

  addProducts(index, product:product){

  }

}
