import { Component, OnInit } from '@angular/core';
import { ServicesService } from '../../services/services.service';
import { Router } from '@angular/router';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css']
})
export class LoginComponent implements OnInit {

  user: string;
  password: string;


  constructor(private service: ServicesService, private router: Router) { }

  ngOnInit() {}

  logar() {
    console.log(this.user);
    console.log(this.password);
    this.service.setHeaderPassAndUser(this.user, this.password);
    this.router.navigate(['/home']);
  }

}
