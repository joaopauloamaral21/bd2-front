import { Injectable } from '@angular/core';
import { Subject, Observable } from '../../../node_modules/rxjs';
import { HttpClient } from '../../../node_modules/@angular/common/http';

@Injectable({
  providedIn: 'root'
})
export class ServicesService {

  header = new Subject();
  webUrl = `http://localhost:8090`;


  constructor(private http: HttpClient) {

  }

  setHeaderPassAndUser(user: string, pass: string) {
    const header = new Headers();
    header.append('usuario', user);
    header.append('senha', pass);
    this.header.next(header);
  }

  getForContext(header: any, context: string) {
    return this.http.get<Array<any>>(`${this.webUrl}/${context}`, {headers: header});
  }

  postForContext(header: any, context: string, data: any) {
    return this.http.post(`${this.webUrl}/${context}/${data.id}`, {data: data, headers: header});
  }

  putForContext(header: any, context: string, data: any) {
    return this.http.put(`${this.webUrl}/${context}/${data.id}`, {data: data, headers: header});
  }

  deleteForContext(header: any, context: string, id: number) {
    return this.http.put(`${this.webUrl}/${context}/${id}`, {headers: header});
  }

  getHeader(sucess: any) {
    return this.header.subscribe(sucess);
  }

}
