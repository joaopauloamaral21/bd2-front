import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';

import { AppComponent } from './app.component';

import { BsDropdownModule } from 'ngx-bootstrap/dropdown';
import { TooltipModule } from 'ngx-bootstrap/tooltip';
import { ModalModule } from 'ngx-bootstrap/modal';
import { RouterModule } from '../../node_modules/@angular/router';
import { AppRouteModule } from './app.route';
import { LoginComponent } from './components/login/login.component';
import { InitComponent } from './components/init/init.component';
import { HttpClientModule } from '@angular/common/http';
import { FormsModule } from '@angular/forms'

@NgModule({
  declarations: [
    AppComponent,
    LoginComponent,
    InitComponent
  ],
  imports: [
    BrowserModule,
    BsDropdownModule.forRoot(),
    TooltipModule.forRoot(),
    ModalModule.forRoot(),
    AppRouteModule,
    HttpClientModule,
    FormsModule
  ],
  providers: [
    AppRouteModule
  ],
  exports: [BsDropdownModule, TooltipModule, ModalModule,RouterModule],
  bootstrap: [AppComponent]
})
export class AppModule { }
